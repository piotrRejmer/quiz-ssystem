--Table structure fot table `questions`

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_text` varchar(300) DEFAULT NULL,
  `question_type_id` int(11) NOT NULL, -- one ansewr or multiple answer
  `level` int(11) NOT NULL, -- basic, advance, expert
  `quiz_id` int(11) ,
   PRIMARY KEY (`id`),
  KEY `quiz_id` (`quiz_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
-- basic 1
-- advance 2
-- expert 3


-- Dumping data for table `questions`
-- only 3 columns
--

INSERT INTO `questions` (`question_text`, `question_type_id`, `level` ) VALUES

  ("Pytanie1",1,1),
  ("Pytanie2",1,1),
  ("Pytanie3",1,1),
  ("Pytanie4",1,1),
  ("Pytanie5",1,1),
  ("Pytanie6",1,1),
  ("Pytanie7",1,1),
  ("Pytanie8",1,1),
  ("Pytanie9",1,1),
  ("Pytanie10",1,1),
  ("Pytanie11",1,1),
  ("Pytanie12",1,1),
  ("Pytanie13",1,1),
  ("Pytanie14",1,1),
  ("Pytanie15",1,1);

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id_answer` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `answer_text` varchar(800) CHARACTER SET utf8 DEFAULT NULL,
  `correct_answer` BOOLEAN NOT NULL,
  PRIMARY KEY (`id_answer`),
  FOREIGN KEY (id) REFERENCES questions(`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
-- correct_answer 0 -false
-- correct_answer 1 -yes

-- Dumping data for table `answers`
-- only 2 columns
--
INSERT INTO `answers` (`id`,`answer_text`, `correct_answer` ) VALUES
  (25,"odp1",false),
  (25,"odp2",false),
  (25,"odp3",true);