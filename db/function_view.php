<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 2015-11-29
 * Time: 09:47
 */

function create_header_html($title)
{
    // wy�wietlenie nag��wka HTML
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Online quiz system</title>

        <!-- Bootstrap -->

        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <div class="container">
    <div class="jumbotron">
        <h1>Online quiz system</h1>
        <p>To become better</p>

    </div>
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">QUIZ.COM</a>
                </div>
                <div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href=index.php>Home</a></li>
                        <li><a href="new_quiz.php">Nowy quiz</a></li>
                        <li><a href="questions.php">Pytania</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <?php
    if($title)
        create_title_html($title);
}

function create_footer_html()
{
    // show footer
    ?>
    <div class="row">
        <div class="col-sm-12 footer" >

            <p>Copyright 2015 REJKI</p>

        </div>
    </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    </body>
    </html>
    <?php
}

function create_title_html($title)
{
    // wy�wietlenie tytu�u
    ?>
    <h2><?php echo $title;?></h2>
    <?php
}
function show_form_new_quiz()
{
    // wyswietlanie formularza do tworzenia nowego quizu
    ?>
    <div class="row">
        <div class="col-sm-12">
            <h3>QUIZ</h3>
            <p>Nowy quiz</p>
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Imię</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" placeholder="Wpisz swoje imię">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd">Ilośc pytań</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="number_questions" placeholder="Wpisz ilośc pytań">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Rozpocznij</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
}
function show_form_add_question()
{
    ?>
    <form class="form-horizontal" role="form" action="questions.php" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="question_text">Treść pytania</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="question_text" name="question_text" placeholder="Wpisz nazwę pytania">
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="hidden" name="add" value="yes">
                        <button type="submit" class="btn btn-default">Dodaj</button>
                    </div>
                </div>
            </form>
    <?php
}

function show_all_questions()
{
    // mozna rozdzielic na dwie funkcje do pobierania i wyswietlania
    $result=take_all_questions();
    $rows = $result->num_rows;
    echo " <div class=\"row\">
        <div class=\"col-sm-12\" >
        <table class=\"table table-hover\">
            <thead>
            <tr>
                <th>ID</th>
                <th>Treść pytania</th>
                <th>Operacja</th>
            </tr></thead>";
    for ($j=0; $j < $rows; ++$j)
    {
        $result->data_seek($j);
        $row=$result->fetch_array(MYSQLI_NUM);
        echo "<tr>";
        echo "<td>".$row[0]."</td>";
        echo "<td>".$row[1]."</td>";
        echo  <<<_END
   <td> <form action="question_edition.php" method="post">
        <input type="hidden" name="edit" value="yes">
        <input type="hidden" name="id" value="$row[0]">
        <input type="hidden" name="question_text" value="$row[1]">
        <td><button type="submit" class="btn btn-success">Edytuj</button></td></form></td>
_END;
        echo  <<<_END
   <td> <form action="questions.php" method="post">
        <input type="hidden" name="delete" value="yes">
  <input type="hidden" name="id" value="$row[0]">
        <td><button type="submit" class="btn btn-danger">Usuń</button></td></form></td>
_END;
        echo "</tr>";
    }
    echo "</table></div></div>";
}
function show_form_delete_question()
{

    ?>
    <form action="questions.php" method="post">
  <input type="hidden" name="delete" value="yes">
  <input type="hidden" name="id" value="<?php $row[0]?>">
        <td><button type="submit" class="btn btn-danger">Usuń</button></td>
    </form>
<?php
}
function show_form_edit_question()
{


    ?>
    <form class="form-horizontal" role="form" action="questions.php" method="post">

                <input  type="hidden" class="form-control"  name="id" value="<?php print $_POST['id'] ?>">


        <div class="form-group">
            <label class="control-label col-sm-2" for="question_text">Treść pytania</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="question_text" name="question_text" value="<?php print $_POST['question_text'] ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="question_type_id">Typ</label>
            <div class="col-sm-10">
                <input type="radio" name="question_type_id" value="1"> Jednokrotnego wyboru<br>
                <input type="radio" name="question_type_id" value="2"> Wielokrotnego wyboru<br>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="level">Poziom</label>
            <div class="col-sm-10">
                <select name="level">
                    <option value="1">Dla początkujących</option>
                    <option value="2">Dla zaawansowanych</option>
                    <option value="3">Dla ekspertów</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="question_text"></label>
            <div class="col-sm-10">
                <input type="checkbox" name="aktywne" value> Aktywne<br>
                <input type="hidden" name="edit" value="yes">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Zapisz</button>
            </div>
        </div>
        Odpowiedzi<br>
        Odpowiedź:
    </form>
<?php
}
function show_form_edit_answers()
{
    // mozna rozdzielic na dwie funkcje do pobierania i wyswietlania
    $id=$_POST['id'];
    $conn=connect_db();
    $query = "select id_answer, answer_text, correct_answer from answers WHERE id='$id'";
    $result=$conn->query($query);
    if(!$result) die($conn->error);
    $rows = $result->num_rows;
    echo " <table class=\"table table-hover\">
            <thead>
            <tr>
                <th>ID</th>
                <th>Treść odpowiedzi</th>
                <th>Poprawna</th>
            </tr>";
    for ($j=0; $j < $rows; ++$j)
    {
        $result->data_seek($j); // co robi metoda data_seek?
        $row=$result->fetch_array(MYSQLI_NUM);
        echo "<tr>";
        echo "<td>".$row[0]."</td>";
        echo "<td><input name='answer_text' value='$row[1]' ></td>";
        echo "<td><input type='checkbox' name='correct_answer' value=''>".$row[2]."</td>";
        echo  <<<_END
   <td> <form action="question_edition.php" method="post">
        <input type="hidden" name="edit_answer" value="yes">
        <input type="hidden" name="id_answer" value="$row[0]">
        <input type="hidden" name="answer_text" value="$row[1]">
        <td><button type="submit" class="btn btn-success">Zapisz</button></td></form></td>
_END;
        echo  <<<_END
   <td> <form action="questions.php" method="post">
        <input type="hidden" name="delete" value="yes">
  <input type="hidden" name="id" value="$row[0]">
        <td><button type="submit" class="btn btn-danger">Usuń</button></td></form></td>
_END;
        echo "</tr>";
    }
    echo "</table>";
    $result->close();
    $conn->close();
}
function show_form_add_answer()
{
    ?>
    <form class="form-horizontal" role="form" action="question_edition.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2" for="answer_text">Dodaj odpowiedź</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="answer_text" name="answer_text" placeholder="Wpisz treść odpowiedzi">
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="hidden" name="add_answer" value="yes">
                <button type="submit" class="btn btn-default">Dodaj</button>
            </div>
        </div>
    </form>
    <?php
}
function show_info()
{
    ?>
    <p>Prosty system testów. Umozliwia wyświetlanie pytań ich edycje i usuwanie. daje możliwość tworzenia testu na podstawie ilosci pytań</p>
<?php
}
