<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 2015-11-29
 * Time: 16:49
 */
require_once('function_all.php');
create_header_html('Edycja pytania');
show_form_edit_question();
if (isset($_POST['edit_answer']) && isset($_POST['answer_text']))
    edit_answer();
if (isset($_POST['add_answer']) && isset($_POST['answer_text']))
    add_answer();
show_form_edit_answers();

show_form_add_answer();
print_r($_POST) ;
create_footer_html();